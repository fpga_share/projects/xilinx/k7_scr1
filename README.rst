K7-SCR1
###############################################################################
Проект портирования SoC `scr1`_ на плату `K7`_.



.. ============================================================================

.. _`scr1`:
   https://github.com/syntacore/scr1.git

.. _`K7`:
   https://aliexpress.ru/item/1005003226113191.html?spm=a2g2w.orderlist.rcmdprod.0.61f74aa66TqxDf&mixer_rcmd_bucket_id=aerabtestalgoRecommendAbV14_testRankingRelevanceYetirank.aerabtestalgoRecommendKazanV3_controlRu1&ru_algo_pv_id=6337bf-46bc9e-a1c6ea-9725ba&scenario=aerUserViewedItemsRcmd&sku_id=12000024744670336&traffic_source=recommendation&type_rcmd=core
